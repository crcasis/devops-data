mysqldump zabbix -u zabbix -p \
        --ignore-table=zabbix.acknowledges \
        --ignore-table=zabbix.alerts \
        --ignore-table=zabbix.auditlog \
        --ignore-table=zabbix.auditlog_details \
        --ignore-table=zabbix.escalations \
        --ignore-table=zabbix.events \
        --ignore-table=zabbix.history \
        --ignore-table=zabbix.history_log \
        --ignore-table=zabbix.history_str \
        --ignore-table=zabbix.history_str_sync \
        --ignore-table=zabbix.history_sync \
        --ignore-table=zabbix.history_text \
        --ignore-table=zabbix.history_uint \
        --ignore-table=zabbix.history_uint_sync \
        --ignore-table=zabbix.trends \
        --ignore-table=zabbix.trends_uint \
        | gzip > backup1_config.sql.gz

mysqldump -u zabbix -p zabbix \
        acknowledges \
        alerts \
        auditlog \
        auditlog_details \
        escalations \
        events \
        history \
        history_log \
        history_str \
        history_text \
        history_uint \
        trends \
        trends_uint \
        --no-data \
       | gzip > backup1_empty_tables.sql.gz