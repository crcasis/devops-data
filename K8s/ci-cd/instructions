Set Up a CI/CD Pipeline with Kubernetes

1.0.- Install Docker on Linux

> curl -fsSL https://get.docker.com/ | sh
After installation, create a Docker group and add the user to this group so you can run 
Docker commands as a non-root user (you’ll need to log out and then log back in after 
running this command):

> sudo usermod -aG docker $USER

Note: When we’re all done, make sure Docker is running:
> sudo service docker start
> docker version

2.0.- Install Kubectl

> curl https://sdk.cloud.google.com | bash
> gcloud init
...
> gcloud components install kubectl

3.0.- Install Prerequisites
- NodeJS
- Git

3.1.- NodeJS
> curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
> sudo apt-get install -y nodejs

3.2.- Git
> sudo apt-get install git
-------------START-TEST--------------
4.0.- Running a test pod
> kubectl run nginx --image nginx --port 80 //creates a deployment
> kubectl expose deployment nginx --type NodePort --port 80  //creates a service
Note: "--port 80" is refered to the port of the container, not the node. To check the
node port, we need to check the created service.
> kubectl get svc nginx
NAME      CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
nginx     10.15.245.51   <nodes>       80:30595/TCP   1m

Note: In this case the port would be 30595. We need to expose this in GCP
> gcloud compute firewall-rules create port30595 --allow tcp:30595
--------------END-TEST---------------
5.0.- Create a Local Image Registry
We’ll set up our own local image registry. We’ll then build, push, and run a sample 
app from the local Docker registry.
> kubectl create -f manifests/registry.yml
> kubectl rollout status deployments/registry

At this point the registry is created. We can build a test image with the following command:
> docker build -t 127.0.0.1:30400/hello-kenzan:latest -f applications/hello-kenzan/Dockerfile applications/hello-kenzan
- Repository: 127.0.0.1:30400/hello-kenzan
- Tag: latest
- f: files
Note: If working on e.g. C9, we need to access to the node on the cluster where "registry" 
pod is runninf to run these commands.
Note: This needs to be done from the directory where Dockerfile is, so if it is the case 
of working on C9, we need to copy all the necessary files to the node.

5.1.- From the same node, let's push the built image.
> docker push 127.0.0.1:30400/hello-kenzan:latest

5.2.- Back to C9 or local environment, let's deply the app.
> kubectl create -f applications/hello-kenzan/k8s/deployment.yaml
Note: To test it is running properly, we need the IP address of the node it is running, 
the port (to open it)

