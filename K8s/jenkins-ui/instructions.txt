This guide will lead us to the point when we have Jenkins UI running on K8s.
__simple__app__ is the second part of this guide, where it shows how to set up
a continuous deployment.

1.- Create a cluster with storage read-write scope.
> gcloud container clusters create jenkins-continuous-deployment \
  --num-nodes 3 \
  --scopes "https://www.googleapis.com/auth/projecthosting,storage-rw"

2.- Create a new namespace
> kubectl create ns jenkins

3.- Creating Jenkins home volume.
> gcloud compute images create jenkins-home-image --source-uri https://storage.googleapis.com/solutions-public-assets/jenkins-cd/jenkins-home-v3.tar.gz
> gcloud compute disks create jenkins-home --image jenkins-home-image

4.0.- Create a Jenkins Deployment and Service. We will need to clone the following repo.
> git clone https://github.com/GoogleCloudPlatform/continuous-deployment-on-kubernetes.git
> cd continuous-deployment-on-kubernetes/

4.1.- Creating a secret for user "jenkins"
Note: The password is going to be the word set instead of "CHANGE_ME", in jenkins/k8s/options file
> kubectl create secret generic jenkins --from-file jenkins/k8s/options -n jenkins

4.2.- Here is when we create the service and the deployment for jenkins
> kubectl create -f jenkins/k8s/

5.0.- At this point service exposes ports 8080 and 50000 for any pods that match the selector.
This will expose the Jenkins web UI and builder/agent registration ports within the Kubernetes cluster.
Additionally the jenkins-ui services is exposed using a NodePort so that our HTTP loadbalancer can reach it.

The Ingress resource is defined in jenkins/k8s/lb/ingress.yaml. We used the Kubernetes secrets API to add 
our certs securely to our cluster and ready for the Ingress to use.

5.1.- Creating the certs (for the HTTPS Load Balancer [Ingress]):

> openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /tmp/tls.key -out /tmp/tls.crt -subj "/CN=jenkins/O=jenkins"
> kubectl create secret generic tls --from-file=/tmp/tls.crt --from-file=/tmp/tls.key --namespace jenkins

5.2.- Creating the Ingress
> kubectl create -f jenkins/k8s/lb
Note: The ingress is going to create a firewall-rule for the open port on the node.
Note: At this point we should be able to access to Jenkins through the UI on
Load Balancer IP address.