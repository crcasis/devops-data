#!/bin/bash

# remove images
docker rmi $(docker images -q)  
# remove containers
docker stop $(docker ps -aq)
docker rm $(docker ps -aq)    
# remove volumes
docker volume rm $(docker volume ls -q)